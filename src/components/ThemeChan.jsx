import { useEffect, useState } from 'react';
import tokens from './tokens.json';



export let changeTheme;

export default function ThemeChan() {
  const [isDarkMode, setIsDarkMode] = useState(false);
  changeTheme = setIsDarkMode

  /* Validacion en caso de que el usuario tenga Dark || Light */
  /* useEffect(() => {
    const isDark = window.matchMedia('(prefers-color-scheme: dark)').matches;
    setIsDarkMode(isDark);
  }, []); */


  const { light, dark, general } = tokens;
  const guide = isDarkMode ? dark : light;

  return (
    <>
      <style jsx global>{`
        :root {
          --primary-color: ${general.primaryColor.value};
          --primary-color-bri: hsl(141, 73%, 62%);
          --color-fuente: ${guide.colorTheme.value};
          --fuente-80: ${guide.fue80.value};
          --fuente-50: ${guide.fue50.value};
          --fuente-20: ${guide.fue20.value};
          --color-contrast: ${guide.colorContrast.value};
          --contrast-20: ${guide.con20.value};
          --contrast-80: ${guide.con80.value};
        }
      `}</style>
    </>
  );
}


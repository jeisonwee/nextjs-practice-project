import Head from 'next/head'
import Header from '../components/Header'
import ThemeChan from '../components/ThemeChan'
import '@/styles/globals.css'

export default function App({ Component, pageProps }) {
  return (
    <>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Practica #1</title>
      </Head>
      <ThemeChan />
      <Header />
      <main className='cont_main'>
        <Component {...pageProps} />
      </main>
    </>
  )
}
